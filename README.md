# README #

A Rust executable to invoke and tinker with contiki_allocator. It is just a good way to debug and execute the allocator before compiling with Contiki and flashing to a device.
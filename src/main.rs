extern crate contiki_allocator;

fn main() {
    let a = Box::new(8); // Allocates memory via our custom allocator crate.
    println!("{}", a);
}
